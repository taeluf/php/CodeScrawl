# ChangeLog

## Noted Changes
- 2023-12-23: Repo cleanup (use non-hidden dirs, etc). Add API readme generation. Add git Changelog template

## Git Log
@template(git/ChangeLog)
