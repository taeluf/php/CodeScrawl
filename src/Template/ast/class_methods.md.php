<?php
/**
 * @param $args[0] the key pointing to the ast, must begin with `class.ClassName`
 * @param $args[1] array class ast
 * @param $args[2] is the AstVerb class instance
 */
$class = $args[1];

$instanceArg = '$'.lcfirst($class['name']);
$visibility = '*';

foreach ($class['methods'] as $m):

    // print_r($m);
// exit;
    $declare = substr($m['declaration'], 0, strpos($m['declaration'], '('));
    $declareParts = explode(' ', $declare);
    if ($visibility!=='*'){
        if (!in_array($visibility, $declareParts))continue;
    }
    $isStatic = false;
    if (in_array('static',$declareParts))$isStatic = true;


    $pos = strpos($m['declaration'],$m['name']);
    $cleanDefinition = substr($m['declaration'],$pos);
    // $pos = strpos($cleanDefinition, '{');
    // $cleanDefinition = trim(substr($cleanDefinition,0,$pos));

    //special declaration conversions:
    // static should be ClassName::method(...)
    // __construct should be new ClassName(...)
    // other should be $className->method(...)

    if ($m['name']=='__construct'){
        $cleanDefinition = $instanceArg.' = new '.$class['name'].substr($cleanDefinition,strlen('__construct'));
    } else if ($isStatic){
        $cleanDefinition = $class['name'].'::'.$cleanDefinition;
    } else {
        $cleanDefinition = $instanceArg.'->'.$cleanDefinition;
    }


    $description = $m['dockblock']['tip']??$m['docblock']['description']??'';
    $description = str_replace("\n", "\n    ", $description);
    $description = trim($description);
?>
- `<?=$cleanDefinition?>`: <?=$description?>

<?php
    foreach ($m['docblock']??[] as $verb=>$text){
        if ($verb=='src'||$verb=='description'||$verb=='type'||$verb=='tip')continue;
        if (is_array($text))continue;
        if (is_array($text))$text = $text['description']??'';
        echo "    - `@$verb`: $text\n";
    }
endforeach;

// print_r($class);
