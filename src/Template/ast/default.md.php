<?php
/**
 * @param $args[0] the key pointing to the ast, must begin with `class.ClassName`
 * @param $args[1] string or array ... whatever the key pointed at 
 * @param $args[2] is the AstVerb class instance
 */
$key = $args[0];

if (is_array($args[1])){
    // $this->warn("Cannot show ast $key.", "it is an array");
    echo "@ast($key) is an array";
} else {
    echo $args[1];
}

