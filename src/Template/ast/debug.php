<?php
/**
 * This template is to debug your mdtemplate
 *
 * @param $args[0] the key pointing to the ast, must begin with `class.ClassName`
 * @param $args[1] string or array ... whatever the key pointed at 
 * @param $args[2] is the AstVerb class instance
 */
$key = $args[0];

ob_start();
var_dump($args[2], true);
$output = ob_get_clean();

echo "---DEBUG_SCRAWL_AST_VERB---"
    ."\nKey:$key"
    ."\nAST Value: \n".print_r($args[1],true)
    ."\nAST Verb class instances: \n".$output;
