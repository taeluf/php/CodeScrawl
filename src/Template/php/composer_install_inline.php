<?php
/**
 * Display composer install instructions for your library
 *
 * @usage `@template(composer_install, vendor/package, ?version)`
 *
 * @param $args[0] package name like `taeluf/code-scrawl`
 * @param $args[1] (optional) branch name ... defaults to current branch
 */


$package = $args[0] ?? \Tlf\Scrawl\Utility\Main::getComposerPackageName();
if ($package==null){
    $this->scrawl->warn("Cannot find composer.json file at ".getcwd().'/composer.json, nor was package name passed to @template(composer_install, vendor/package)');
    echo "--cannot print composer install instructions because package name was not passed--";
    return;
}
$version = $args[1] ?? \Tlf\Scrawl\Utility\Main::getCurrentBranchForComposer();

echo "`composer require $package $version`";
