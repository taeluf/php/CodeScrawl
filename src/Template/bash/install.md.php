<?php
/**
 * @param $args[0] the name of the executable command
 * @param $args[1] relative path to executable file within the repo
 */
$command=$args[0];
$execFile=$args[1];
$git_clone_url = \Tlf\Scrawl\Utility\Main::getGitCloneUrl();

?>
```bash
pwd="$(pwd)";
command="<?=$command?>"
downloadDir=~/.gitclone
mkdir -p "$downloadDir"
cd "$downloadDir"
git clone <?=$git_clone_url?> ${command} 
echo "alias ${command}=\"${downloadDir}/${command}/<?=$execFile?>\"" >> ~/.bashrc
chmod ug+x "${downloadDir}/${command}/<?=$execFile?>"
cd "$pwd";
source ~/.bashrc
echo "You can now run \`${command}\`"
```
