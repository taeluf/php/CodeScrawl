<?php

namespace Tlf\Scrawl;

/**
 * An extensions has multiple opportunities to interact in the documentation generation process
 */
interface Extension {
    
    /**
     * Initialize the extension
     * @param $scrawl a Scrawl instance
     */
    public function __construct(\Tlf\Scrawl $scrawl);

    /**
     * Any initial setup required. This is called AFTER `file.bootstrap` files are loaded, and before anything else.
     */
    public function bootstrap();


    /**
     * Called when an AST has been generated, after it has been set to scrawl.
     * To change the AST scrawl is using, call `$scrawl->set('ast', "class.{$className})"
     *
     * @param $className fully qualified class name
     * @param $ast an ast as an array
     */
    public function ast_generated(string $className, array $ast);

    /** 
     * ASTs have been generated and api documentation has been output
     * @param $asts the root array containing all the ASTs
     */
    public function astlist_generated(array $asts);

    /**
     * Called before any files are processed
     * @param $code_files array of all files to be scanned for code
     */
    public function scan_filelist_loaded(array $code_files);

    /**
     * Called when an individual file is finished being processed
     *
     * @param $path absolute path to the file
     * @param $relPath relative path to the file
     * @param $file_content the content of the file
     * @param $file_exports array of all items exported from just this file
     */
    public function scan_file_processed(string $path, string $relPath, string $file_content, array $file_exports);

    /**
     * Called when all files are finished being processed
     *
     * @param $code_files array of all files that were scanned and processed
     * @param $all_exports array of all exports found in the scanned files
     */
    public function scan_filelist_processed(array $code_files, array $all_exports);

    /**
     * Called when all documentations ource files have been loaded
     *
     * @param $doc_files array of all documentation source files, containing Code Scrawl code
     * @param $mdverb_ext \Tlf\Scrawl\Ext\MdVerbs instance
     */
    public function doc_filelist_loaded(array $doc_files, \Tlf\Scrawl\Ext\MdVerbs $mdverb_ext);

    /**
     * Called when an individual documentation source file has been loaded, before any processing
     *
     * @param $path absolute path to the file
     * @param $relPath relative path to the input file, relative within the documentation source dir
     * @param $file_content the content of the file
     */
    public function doc_file_loaded($path,$relPath,$file_content);

    /**
     * Called when an individual documentation source file has been processed and all `@mdverbs` have been replaced, and the file has been written to disk
     *
     * @param $path absolute path to the output file 
     * @param $relPath relative path to the output file, relative within the documentation dir
     * @param $file_content the content of the file
     */
    public function doc_file_processed($path,$relPath,$file_content);

    /**
     * Called when all documentation source files have been processed, before readme.md is copied over to root dir.
     *
     * @param $doc_files array of all documentation source files, containing Code Scrawl code
     */
    public function doc_filelist_processed($doc_files);

    /**
     * Called after all other scrawl operations are complete
     */
    public function scrawl_finished();
}
