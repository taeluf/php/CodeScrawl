<?php

namespace Tlf\Scrawl\OldStuffs;

/**
 * just old code that i don't wanna get rid of
 */
class ugh_old_things{

    public function run_init($cli, $args){
        if (
            $this->prompt("Create sample files for code scrawl in '".$cli->pwd."'? (y/n)", "y")
            !="y"
        ) {
            return false;
        } 

        $files = \Tlf\Scrawl\Utility::allFilesFromDir(dirname(__DIR__).'/test/input/Project/', '', []);
        foreach ($files as $f){
            if (substr($f->relPath,0,6)=='/docs/')continue;
            $file = $cli->pwd.'/'.$f->relPath;
            $dir = dirname($file);
            if (!is_dir($dir))mkdir($dir);
            if (!is_file($file)){
                file_put_contents($file, $f->content());
            }
        }

    }

    public function prompt($message, $default){
        if ($this->configs['noprompt'][0] === true) {
            return $default;
        }

        return readline($message);
    }


    public function pathToRootFrom($configuredDirectory){
        $relPath = $this->getConfig('dir.'.$configuredDirectory);
        if ($relPath===null) return null;
        $relPath = $relPath[0];
        $relPath = str_replace('\\','/',$relPath);
        $parts = explode('/', $relPath);
        $retRelPath = '';
        foreach ($parts as $p){
            $retRelPath .= '../';
        }
        return $retRelPath;
    }
}
