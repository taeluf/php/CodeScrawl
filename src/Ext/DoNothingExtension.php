<?php

namespace Tlf\Scrawl;

/**
 * Literally does nothing except the constructor and $scrawl property. Just a base class to save boilerplate on the interface
 */
class DoNothingExtension implements \Tlf\Scrawl\Extension {


    /**
     * a scrawl instance
     */
    protected \Tlf\Scrawl $scrawl;

    public function __construct(\Tlf\Scrawl $scrawl){
        $this->scrawl = $scrawl;
        $this->scrawl->good("TestExtension::__construct", "good");
    }

    public function bootstrap(){}

    public function ast_generated(string $className, array $ast){}

    public function astlist_generated(array $asts) {}

    public function scan_filelist_loaded(array $code_files){ }

    public function scan_file_processed(string $path, string $relPath, string $file_content, array $file_exports){ }

    public function scan_filelist_processed(array $code_files, array $all_exports){ }

    public function doc_filelist_loaded(array $doc_files, \Tlf\Scrawl\Ext\MdVerbs $mdverb_ext){ }

    public function doc_file_loaded($path,$relPath,$file_content){ }

    public function doc_file_processed($path,$relPath,$file_content){ }

    public function doc_filelist_processed($doc_files){ }

    public function scrawl_finished(){ }


}
