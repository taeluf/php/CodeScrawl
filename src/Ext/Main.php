<?php

namespace Tlf\Scrawl\Ext;

class Main {

    public function copy_readme($scrawl){
        $content = $scrawl->read_file('README.md');
        $scrawl->write_doc('README.md', $content);
    }

    // public function write_exports_list(){
    //     // I'm not planning to re-implement this ... though i am leaving around the old code in case i change my mind
    // }

}

