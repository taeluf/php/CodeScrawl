<?php

namespace Tlf\Scrawl;

/**
 * Class purely exists to test that extensions work
 */
class TestExtension implements \Tlf\Scrawl\Extension {


    /**
     * a scrawl instance
     */
    protected \Tlf\Scrawl $scrawl;

    public function __construct(\Tlf\Scrawl $scrawl){
        $this->scrawl = $scrawl;
        $this->scrawl->good("TestExtension::__construct", "good");
    }

    public function bootstrap(){
        $this->scrawl->good("TestExtension::bootstrap()","good");
    }


    public function ast_generated(string $className, array $ast){
        $this->scrawl->good("TestExtension::ast_generated()","good");
    }

    public function astlist_generated(array $asts) {
        $this->scrawl->good("TestExtension::astlist_generated()","good");
    }

    public function scan_filelist_loaded(array $code_files){ 
        $this->scrawl->good("TestExtension::scan_filelist_loaded()","good");

    }

    public function scan_file_processed(string $path, string $relPath, string $file_content, array $file_exports){ 
        $this->scrawl->good("TestExtension::scan_file_processed()","good");
    }

    public function scan_filelist_processed(array $code_files, array $all_exports){ 
        $this->scrawl->good("TestExtension::scan_filelist_processed()","good");

    }

    public function doc_filelist_loaded(array $doc_files, \Tlf\Scrawl\Ext\MdVerbs $mdverb_ext){ 
        $this->scrawl->good("TestExtension::doc_filelist_loaded()","good");

    }

    public function doc_file_loaded($path,$relPath,$file_content){ 
        $this->scrawl->good("TestExtension::doc_file_loaded()","good");

    }

    public function doc_file_processed($path,$relPath,$file_content){ 
        $this->scrawl->good("TestExtension::doc_file_processed()","good");

    }

    public function doc_filelist_processed($doc_files){ 
        $this->scrawl->good("TestExtension::doc_filelist_processed()","good");

    }

    public function scrawl_finished(){ 
        $this->scrawl->good("TestExtension::scrawl_finished()","good");

    }


}
