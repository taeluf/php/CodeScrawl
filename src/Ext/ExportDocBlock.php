<?php

namespace Tlf\Scrawl\FileExt;

/**
 * Export docblock content above `@export(key)`
 * @featured
 */
class ExportDocBlock {

    protected $regs = [
        'export.key' => '/\@export\(([^\)]*)\)/',
        'Exports' => '/((?:.|\n)*) *(\@export.*)/',
    ];

    /**
     * get an array of docblocks
     * @return array of docblocks, like `[0=>['raw'=>'/*...', 'clean'=>'...'], 1=>...]`
     */
    public function get_docblocks(string $str): array{
        $blocks = \Tlf\Scrawl\Utility\DocBlock::extractBlocks($str);
        return $blocks;
    }

    /**
     * get an array of exported text
     * @param $docblocks Array of docblocks. @see(get_docblocks())
     */
    public function get_exports(array $docblocks){
        $exports = [];
        foreach ($docblocks as $db){
            
            $comment = \Tlf\Scrawl\Utility\Main::trimTextBlock($db->clean);
            $reg = $this->regs['Exports'];
            $did_match = preg_match($reg, $comment, $matches);
            if (!$did_match)continue;

            $exported = \Tlf\Scrawl\Utility\Main::trimTextBlock($matches[1]);

            $key_portion = $matches[2];
            $did_match = preg_match($this->regs['export.key'], $key_portion, $key_matches);
            if (!$did_match)continue;
            $key = $key_matches[1];
            $exports[$key] = $exported;
        }

        return $exports;
    }

}
