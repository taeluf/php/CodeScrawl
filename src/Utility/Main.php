<?php

namespace Tlf\Scrawl\Utility;

class Main {

    static protected $classMap=[];

    static protected $isRegistered=false;

    static public function spl_autoload($class){
        $class = '\\'.$class;
        if (!isset(static::$classMap[$class]))return;
        $file = static::$classMap[$class];
        unset(static::$classMap[$class]);
        require($file);
    }
    static public function autoload($dir){
        if (!static::$isRegistered){
            static::$isRegistered = true;
            spl_autoload_register([get_class(), 'spl_autoload']);
            require_once(__DIR__.'/Php.php');
        }
        $files = static::allFilesFromDir($dir, '', ['.php']);
        
        foreach ($files as $f){
            $class = Utility\Php::getClassFromFile($f->path);
            if (trim($class)=='')continue;
            // require_once($f->path);
            static::$classMap[$class] = $f->path;
        }

        // print_r(static::$classMap);
    }
    /** trim a block of text
     */
    static public function trimTextBlock($textBlock){
        $textBlock = static::removeLeftHandPad($textBlock);
        //remove blank lines at beginning
        $textBlock = preg_replace('/^(\s*\n)+/','',$textBlock);
        //remove blank lines at end
        $textBlock = preg_replace('/(\n\s*)+$/','',$textBlock);
        return $textBlock;
    }

    /**
     *  Trim the leading spaces from a block of text
     */
    static public function removeLeftHandPad($textBlock){
        $leftPadList = [];
        $leftPadReg = '/^(\ *)[^\s\n\r]/m';
        preg_match_all($leftPadReg,$textBlock,$leftPadList);

        $shortest = null;
        foreach ($leftPadList[1] as $pad){
            if ($shortest===null
                ||strlen($pad)<strlen($shortest)
            ){
                $shortest = $pad;
            }
        }
        $textUnpadded = preg_replace("/^{$shortest}/m",'',$textBlock);
        return $textUnpadded;
    }

    static public function allFilesFromDir(string $rootDir, string $relDir, array $forExt=[]){
        $dir = $rootDir.'/'.$relDir;
        if (!is_dir($dir)){
            return [];
        }
        $dh = opendir($dir);
        $allFiles = [];
        while ($file=readdir($dh)){
            if ($file=='.'||$file=='..')continue;
            if (is_dir($dir.'/'.$file)){
                $subFiles = self::allFilesFromDir($rootDir, $relDir.'/'.$file,$forExt);
                $allFiles = array_merge($allFiles,$subFiles);
                continue;
            }  
            $include = false;
            if ($forExt===[]||in_array('*', $forExt)){
                $include = true;
            } else {
                foreach ($forExt as $ext){
                    $len = strlen($ext);
                    if (strtolower(substr($file, -$len))===strtolower($ext)){
                        $include = true;
                        break;
                    }
                }
            }
            // $allFiles[] = str_replace('//','/',$dir.'/'.$file);
            if ($include){
                // $allFiles[] = new File($rootDir, $relDir.'/'.$file);
                $allFiles[] = $relDir.'/'.$file;
            }
        }
        return $allFiles;
    }

    static public function DANGEROUS_removeNonEmptyDirectory($directory){
        $src = $directory;
        $dir = opendir($src);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                $full = $src . '/' . $file;
                if ( is_dir($full) ) {
                    static::DANGEROUS_removeNonEmptyDirectory($full);
                }
                else {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }


    /**
     * Check your composer.json in the current working directory for a `"name"`
     * @return a string like taeluf/code-scrawl or null
     */
    static public function getComposerPackageName(){
        $file = getcwd().'/composer.json';
        if (!file_exists($file))return null;
        $settings = json_decode(file_get_contents($file), true);
        return $settings['name'];
    }

    static public function getCurrentBranchForComposer(){
        $branch = exec("git branch --show-current");
        return $branch.'.x-dev';
    }


    /**
     * Return the git clone url, but always return the https version
     *
     * @return string https git url to git clone the current project
     */
    static public function getGitCloneUrl(): string {
        $url = exec("git config --get remote.origin.url | sed -r 's/.*(\\@|\\/\\/)([^:\\/]*)(\\:|\\/)(.*)\\.git/https:\\/\\/\\2\\/\\4/'");
        $url .= '.git';
        return $url;
    }
}
