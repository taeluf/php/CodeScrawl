<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# All Classes  
Documentation generated by @easy_link(tlf, php/code-scrawl)  
  
## Tlf\Scrawl\Ext\MdVerb\MainVerbs    
no docblock    
See [MainVerbs.php](/docs/api/src/MdVerb/MainVerbs.php.md) for more.  
  
  
## Tlf\Scrawl\Ext\MdVerbs    
Runs `@mdverb` extensions, enabling special callbacks for `@verb()`s in `.src.md` files    
See [MdVerbs.php](/docs/api/src/MdVerb/MdVerbs.php.md) for more.  
  
  
## Tlf\Scrawl\Ext\MdVerb\Ast    
no docblock    
See [Ast.php](/docs/api/src/MdVerb/AstVerb.php.md) for more.  
  
  
## Tlf\Scrawl\VersionBumper    
Bumps versions. This implementation may be moved to its own, separate library. Idk.    
See [VersionBumper.php](/docs/api/src/VersionBumper.php.md) for more.  
  
  
## Tlf\Scrawl\FileExt\ExportDocBlock    
Export docblock content above `@export(key)`  
@featured    
See [ExportDocBlock.php](/docs/api/src/Ext/ExportDocBlock.php.md) for more.  
  
  
## Tlf\Scrawl\Extension\Notes    
Literally does nothing except the constructor and $scrawl property. Just a base class to save boilerplate on the interface  
    
See [Notes.php](/docs/api/src/Ext/Notes.php.md) for more.  
  
  
## Tlf\Scrawl\DoNothingExtension    
Literally does nothing except the constructor and $scrawl property. Just a base class to save boilerplate on the interface    
See [DoNothingExtension.php](/docs/api/src/Ext/DoNothingExtension.php.md) for more.  
  
  
## Tlf\Scrawl\FileExt\ExportStartEnd    
Export code between `// @export_start(key)` and `// @export_end(key)`  
@featured    
See [ExportStartEnd.php](/docs/api/src/Ext/ExportStartEnd.php.md) for more.  
  
  
## Tlf\Scrawl\Ext\Main    
no docblock    
See [Main.php](/docs/api/src/Ext/Main.php.md) for more.  
  
  
## Tlf\Scrawl\TestExtension    
Class purely exists to test that extensions work    
See [TestExtension.php](/docs/api/src/Ext/TestExtension.php.md) for more.  
  
  
## Tlf\Scrawl\FileExt\Php    
Integrate the lexer for PHP files    
See [Php.php](/docs/api/src/Ext/Php.php.md) for more.  
  
  
## Tlf\Scrawl    
Central class for running scrawl.    
See [Scrawl.php](/docs/api/src/Scrawl.php.md) for more.  
  
  
## Tlf\Scrawl\OldStuffs\ugh_old_things    
just old code that i don't wanna get rid of    
See [ugh_old_things.php](/docs/api/src/Old.php.md) for more.  
  
  
## Tlf\Scrawl\Utility\Regex    
no docblock    
See [Regex.php](/docs/api/src/Utility/Regex.php.md) for more.  
  
  
## Tlf\Scrawl\Utility\DocBlock    
@featured    
See [DocBlock.php](/docs/api/src/Utility/DocBlock.php.md) for more.  
  
  
## Tlf\Scrawl\Utility\Main    
no docblock    
See [Main.php](/docs/api/src/Utility/Main.php.md) for more.  
  
  
## Tlf\Scrawl\Test\ExportStartEnd    
no docblock    
See [ExportStartEnd.php](/docs/api/test/run//ExportStartEnd.php.md) for more.  
  
  
## Tlf\Scrawl\Test\Main    
no docblock    
See [Main.php](/docs/api/test/run//Main.php.md) for more.  
  
  
## Tlf\Scrawl\Test\MdDocs    
For testing all things `.md` file    
See [MdDocs.php](/docs/api/test/run//MdDocs.php.md) for more.  
  
  
## Tlf\Scrawl\Test\MdTemplates    
For testing all things `.md` file    
See [MdTemplates.php](/docs/api/test/run//MdTemplates.php.md) for more.  
  
  
## Tlf\Scrawl\Test\Integrate    
no docblock    
See [Integrate.php](/docs/api/test/run//Integrate.php.md) for more.  
  
  
## Tlf\Scrawl\Test\SrcCode    
For testing all things related to source code    
See [SrcCode.php](/docs/api/test/run//SrcCode.php.md) for more.  
  
  
  
