<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Notes in this project's code  
Notes are marked in code by writing `@NOTE`. Everything on the line after it is captured.  
[line number] Trimmed Text of the line  
  
## Notes for src/Ext/Notes.php  
[Docs](/api/src/Ext/Notes.php.md)  
[Source Code](/src/Ext/Notes.php)  
  
- [8] this notes extension should be updated to handle TODOs as well!  
- [41] next line lists 'comment-characters' that are allowed prior to an @NOTE  
- [74] This section outputs Notes.md & contains the explainer text  
  
## Notes for bin//scrawl  
[Docs](/api/bin//scrawl.md)  
[Source Code](/bin//scrawl)  
  
- [51] config file locations are `.config/scrawl.json`, `.docsrc/config.json`, or `config/scrawl.json`  
  
## Notes for bin//scrawl.phar  
[Docs](/api/bin//scrawl.phar.md)  
[Source Code](/bin//scrawl.phar)  
  
- [79710] If the directive stack is empty, creates a new directive layer  
- [102997] `$varDelim` is a string of characters used to separate dynamic portions of a url.  
- [102998] `$varDelim` is a global setting, but you can change it, add routes, change it, add routes, etc.  
- [102999] `pattern` refers to an unparsed url pattern like `/{category}/{blog_name}/`  
- [103000] added routers take a `\Lia\Obj\Request` object  
- [103001] route derivers accept `false` and return array of patterns or a `\Lia\Obj\Request` object  
- [103002] test patterns (or testReg) are simplified patterns with `?` in place of dynamic paramater names & are used internally  
- [103003] `$routeMap` contains all the info needed for selecting which route to use  
- [103004] optional paramaters (`/blog/{?optional}/`) only allow for one optional paramater  
- [103043] `$varDelim` is a string of characters used to separate dynamic portions of a url.  
- [103044] `$varDelim` is a global setting, but you can change it, add routes, change it, add routes, etc.  
- [103045] `pattern` refers to an unparsed url pattern like `/{category}/{blog_name}/`  
- [103046] added routers take a `\Lia\Obj\Request` object  
- [103047] route derivers accept `false` and return array of patterns or a `\Lia\Obj\Request` object  
- [103048] test patterns (or testReg) are simplified patterns with `?` in place of dynamic paramater names & are used internally  
- [103049] `$routeMap` contains all the info needed for selecting which route to use  
- [103050] optional paramaters (`/blog/{?optional}/`) only allow for one optional paramater  
- [103066] `$routeMap` contains all the info needed for selecting which route to use  
- [103067] optional paramaters (`/blog/{?optional}/`) only allow for one optional paramater  
- [103162] this setting is global across all routes. You can circumvent this by using a second router addon