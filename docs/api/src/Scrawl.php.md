<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Scrawl.php  
  
# class Tlf\Scrawl  
Central class for running scrawl.  
See source code at [/src/Scrawl.php](/src/Scrawl.php)  
  
## Constants  
  
## Properties  
- `public array $stuff = [];` array for get/set  
- `public array $extensions = [  
        'code'=>[],  
    ];`   
- `public array $ScrawlExtensions = [];` Array of \Tlf\Scrawl\Extension objects  
- `public \Tlf\Scrawl\Ext\MdVerbs $mdverb_ext;` \Tlf\Scrawl\Ext\MdVerbs  
- `public string $file_bootstrap = null;` absolute path to php file to `require()` before scrawl runs  
- `public string $dir_docs = null;` absolute path to your documentation dir  
- `public string $dir_root = null;` absolute path to the root of your project  
- `public string $dir_src = null;` absolute path to the documentation source dir  
- `public array $dir_scan = [];` array of relative path to dirs to scan, within your dir_root  
- `public array $verb_handlers = [];` array of handlers for the mdverb extension  
- `public array $template_dirs = [];` absolute path to directories that contain md templates  
- `public bool $markdown_preserveNewLines = true;` if true, append two spaces to every line so all new lines are parsed as new lines  
- `public bool $markdown_prependGenNotice = true;` if true, add an html comment to md docs saying not to edit directly  
- `public bool $readme_copyFromDocs = true;` if true, copies `docs/README.md` to project root `README.md`  
- `public bool $deleteExistingDocs = false;` If true will delete all files in your docs dir before running  
- `public string $api_output_dir = 'api/';` Which directory to write Class/Method/Property info to  
- `public bool $api_generate_readme = true;` True to generate a README listing all classes, inside the api_output_dir  
- `public array $options = [];` Array of values, typically passed in through cli  
  
## Methods   
- `public function __construct(array $options=[])`   
- `public function parse_rel_path(string $base_path, string $target_path, bool $use_realpath = true): string` Get the relative path within target_path, if it starts with root_path  
  
- `public function get_doc_path(\Tlf\Cli $cli, array $args): string` Cli function to get the absolute path to a code file  
  
- `public function get_doc_source_path(\Tlf\Cli $cli, array $args): string` Cli function to get the absolute path to a documentation source file for a .md file   
  
- `public function get_template(string $name, array $args)` Get a template stored on disk. `$name` should be relative path without extension. `$args` is passed to template code, but not `extract`ed. Template files must end with `.md.php` or just `.php`.  
- `public function get(string $group, string $key)`   
- `public function get_group(string $group)`   
- `public function set(string $group, string $key, $value)`   
- `public function parse_str($str, $ext)`   
- `public function write_doc(string $rel_path, string $content)` save a file to disk in the documents directory  
- `public function write_file(string $rel_path, string $content)` save a file to disk in the root directory  
- `public function read_file(string $rel_path)` Read a file from disk, from the project root  
- `public function read_doc(string $rel_path)` Read a file from disk, from the project docs dir  
- `public function doc_path(string $rel_path)` get a path to a docs file  
- `public function report(string $msg)` Output a message to cli (may do logging later, idk)  
- `public function warn($header, $message)` Output a message to cli, header highlighted in red  
- `public function good($header, $message)` Output a message to cli, header highlighted in red  
- `public function prepare_md_content(string $markdown)` apply small fixes to markdown  
- `public function get_all_docsrc_files()`   
- `public function get_all_scan_files(): array` get array of all files in `$scrawl->dir_scan`   
- `public function generate_apis()` Generate api docs for all files  
(currently only php files)  
- `public function generate_apis_readme()` Create a README file that lists all of the classes in the API dir.  
- `public function generate_api($rel_path)` Generate api doc for a single file  
(currently only php files)  
  
- `public function get_all_classes(): array` Get an array of all classes scanned within this repo.   
  
- `public function get_all_traits(): array` Get an array of all classes scanned within this repo.   
  
- `public function setup_extensions(array $extension_classes)` Array of \Tlf\Scrawl\Extension objects  
- `public function run()` Execute scrawl in its entirety  
- `public function get_ast(string $file): array` get an array ast from a file  
Currently only supports php files  
Also sets the ast to scrawl  
- `public function setup_mdverb_ext()` get the class ast  
  
