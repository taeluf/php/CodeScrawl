<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Ext/ExportStartEnd.php  
  
# class Tlf\Scrawl\FileExt\ExportStartEnd  
Export code between `// @export_start(key)` and `// @export_end(key)`  
@featured  
See source code at [/src/Ext/ExportStartEnd.php](/src/Ext/ExportStartEnd.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function __construct()`   
- `public function get_exports(string $str): array` Parse a string containing blocks marked by @export_start(key) and @export_end(key). Extract every line in between those two lines. return an array containing key=>value pairs for these exported sections.  
  
