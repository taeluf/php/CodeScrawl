<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Ext/ExportDocBlock.php  
  
# class Tlf\Scrawl\FileExt\ExportDocBlock  
Export docblock content above `@export(key)`  
@featured  
See source code at [/src/Ext/ExportDocBlock.php](/src/Ext/ExportDocBlock.php)  
  
## Constants  
  
## Properties  
- `protected $regs = [  
        'export.key' => '/\@export\(([^\)]*)\)/',  
        'Exports' => '/((?:.|\n)*) *(\@export.*)/',  
    ];`   
  
## Methods   
- `public function get_docblocks(string $str): array` get an array of docblocks  
- `public function get_exports(array $docblocks)` get an array of exported text  
  
