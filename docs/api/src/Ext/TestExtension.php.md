<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Ext/TestExtension.php  
  
# class Tlf\Scrawl\TestExtension  
Class purely exists to test that extensions work  
See source code at [/src/Ext/TestExtension.php](/src/Ext/TestExtension.php)  
  
## Constants  
  
## Properties  
- `protected \Tlf\Scrawl $scrawl;` a scrawl instance  
  
## Methods   
- `public function __construct(\Tlf\Scrawl $scrawl)`   
- `public function bootstrap()`   
- `public function ast_generated(string $className, array $ast)`   
- `public function astlist_generated(array $asts)`   
- `public function scan_filelist_loaded(array $code_files)`   
- `public function scan_file_processed(string $path, string $relPath, string $file_content, array $file_exports)`   
- `public function scan_filelist_processed(array $code_files, array $all_exports)`   
- `public function doc_filelist_loaded(array $doc_files, \Tlf\Scrawl\Ext\MdVerbs $mdverb_ext)`   
- `public function doc_file_loaded($path,$relPath,$file_content)`   
- `public function doc_file_processed($path,$relPath,$file_content)`   
- `public function doc_filelist_processed($doc_files)`   
- `public function scrawl_finished()`   
  
