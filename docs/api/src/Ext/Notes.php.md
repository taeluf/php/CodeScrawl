<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Ext/Notes.php  
  
# class Tlf\Scrawl\Extension\Notes  
Literally does nothing except the constructor and $scrawl property. Just a base class to save boilerplate on the interface  
  
See source code at [/src/Ext/Notes.php](/src/Ext/Notes.php)  
  
## Constants  
  
## Properties  
- `protected array $notes = [];` notes: <string rel_file_path, array notes_within_file>  
notes_within_file: <int index, array note>  
note: ['file'=>string, 'line_num'=>int, 'line'=>string]. line is trimmed  
  
## Methods   
- `public function scan_file_processed(string $path, string $relPath, string $file_content, array $file_exports)` Record each @NOTE line with it's file path & line number information  
  
Called when an individual file is finished being processed  
  
- `public function scan_filelist_processed(array $code_files, array $all_exports)` Write the Notes.md file  
  
Called when all files are finished being processed  
  
  
