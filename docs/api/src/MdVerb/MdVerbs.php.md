<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/MdVerb/MdVerbs.php  
  
# class Tlf\Scrawl\Ext\MdVerbs  
Runs `@mdverb` extensions, enabling special callbacks for `@verb()`s in `.src.md` files  
See source code at [/src/MdVerb/MdVerbs.php](/src/MdVerb/MdVerbs.php)  
  
## Constants  
  
## Properties  
- `protected $regs = [  
          
        'verb' =>'/(?<!\\\\)\@([a-zA-Z_]+)\(([^\)]*)\)/m',  
    ];`   
- `public $handlers = [];` array of verb handlers  
  
- `public \Tlf\Scrawl $scrawl;`   
  
## Methods   
- `public function __construct(\Tlf\Scrawl $scrawl=null)`   
- `public function get_verbs(string $str): array` Get all `@verbs(arg1, arg2)`   
- `public function replace_all_verbs(string $doc): string` Get all verbs, execute them, and replace them within the doc  
- `public function run_verb(string $src, string $verb, array $args): string` Execute a verb handler and get its output   
  
  
