<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/MdVerb/MainVerbs.php  
  
# class Tlf\Scrawl\Ext\MdVerb\MainVerbs  
  
See source code at [/src/MdVerb/MainVerbs.php](/src/MdVerb/MainVerbs.php)  
  
## Constants  
  
## Properties  
- `public \Tlf\Scrawl $scrawl;` a scrawl instance  
- `protected array $see_file_failures = [];` array<string absolute_file_path, string relPath> array of files that didn't exist when see_file() was called  
  
## Methods   
- `public function __construct(\Tlf\Scrawl $scrawl)`   
- `public function setup_handlers(\Tlf\Scrawl\Ext\MdVerbs $md_ext)` add callbacks to `$md_ext->handlers`  
- `public function at_system(string $system_command, ...$options)` Run a command on the computer's operating system.  
  
Supported options: `trim`, `trim_empty_lines`, `last_x_lines, int`  
  
- `public function at_template(string $templateName, ...$templateArgs)` Load a template  
- `public function at_import(string $key)` Import something previously exported with `@export` or `@export_start/@export_end`  
  
- `public function at_file(string $relFilePath)` Copy a file's content into your markdown.  
  
- `public function at_see_files(...$files)` Outputs multiple markdown links separated by comma. Each argument is a `path;Name` combo, separated by a semicolon. The name is optional.  
Shorthand for multiple calls to `@see_file()`.   
  
- `public function at_see_file(string $relFilePath, string $link_name = null)` Get a link to a file in your repo  
  
- `public function at_hard_link(string $url, string $name=null)` just returns a regular markdown link. In future, may check validity of link or do some kind of logging  
- `public function at_link(string $link_name, string $alternative_text = null)` Output links configured in your config json file.  
Config format is `{..., "links": { "link_name": "https://example.org"} }`  
  
- `public function at_easy_link(string $service, string $target)` Get a link to common services (twitter, gitlab, github, facebook)  
  
- `public function bootstrap()`   
- `public function ast_generated(string $className, array $ast)`   
- `public function astlist_generated(array $asts)`   
- `public function scan_filelist_loaded(array $code_files)`   
- `public function scan_file_processed(string $path, string $relPath, string $file_content, array $file_exports)`   
- `public function scan_filelist_processed(array $code_files, array $all_exports)`   
- `public function doc_filelist_loaded(array $doc_files, \Tlf\Scrawl\Ext\MdVerbs $mdverb_ext)`   
- `public function doc_file_loaded($path,$relPath,$file_content)`   
- `public function doc_file_processed($path,$relPath,$file_content)`   
- `public function doc_filelist_processed($doc_files)`   
- `public function scrawl_finished()`   
  
