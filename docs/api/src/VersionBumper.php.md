<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/VersionBumper.php  
  
# class Tlf\Scrawl\VersionBumper  
Bumps versions. This implementation may be moved to its own, separate library. Idk.  
See source code at [/src/VersionBumper.php](/src/VersionBumper.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function get_new_version_from_git(array $bump_rules): string` Use system git commands to determine current version from the branch name and available tags.  
  
You should run `git fetch` before using this for accurate results.  
  
- `public function get_new_version(string $latest_version, string $latest_commit_msg, array $bump_rules): string` Get a bumped version number. Returns `$latest_version` if none of the `$bump_rules` apply.  
  
Ex: 0.8.0 gets commit 'bugfix: something', and bumps to 0.8.0.1 based on the bump rules provided.  
Ex: 0.8.0.2 gets commit 'feature: cool stuff', and bumps to 0.8.1.0 based on the bump rules provided.  
Ex: 0.8.0 gets commit 'notes', and does not bump, so '0.8.0' is returned.  
  
- `public function get_index_to_bump(string $commit_message, array $index_rules): int`   
- `public function get_latest_tag(string $version_prefix=''): string`   
- `public function get_current_branch(): string` Return the name of the current git branch  
- `public function get_branch_version(string $branch_name): string` Assumes branch name starts with 'v', removes it, and returns the remaining string, assumed to be numbers separated by periods.  
  
  
