<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run//ExportStartEnd.php  
  
# class Tlf\Scrawl\Test\ExportStartEnd  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testExportsBug1()` The old preg_match_all solution was buggy and provided unexpected results in at least one case. Rather than troubleshoot the regex, I implemented a much more procedural PHP strpos looping solution.   
  
