<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run//MdDocs.php  
  
# class Tlf\Scrawl\Test\MdDocs  
For testing all things `.md` file  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testAstVerb()`   
- `public function testMdVerbsMain()`   
- `public function testMdVerbsParsing()`   
- `public function testCopyReadme()`   
  
