# Code Scrawl Status

## Jan 28, 2024
Replaced the old export start/end code with a PHP-solution due to a bug I found (no longer using regex). added a test to troubleshoot the bug.

## Aug 21, 2024
I forgot to write notes yesterday (and just used git for them today):
- c0aff23  (HEAD -> v0.8) add Notes extension. A simple version is fully implemented & outputs a decent Notes.md file. The 1.0 configuration has it added to default extensions. Readme is updated with extensions listed [4 seconds ago]  
- 9829e89  implementing @NOTE feature [32 minutes ago]  
- a38b190  (origin/v0.8) generate changelog docs no longer contain non-breaking ascii joiner [23 hours ago]  
- 76259ad  add Documentation index to top of readme. Add 'generate changelog' to usage docs [23 hours ago]  
- c575a05  add link to documentation to the default configuration json displayed in the readme [24 hours ago]  
- c2fc02e  run scrawl [31 hours ago]  
- 5587a01  fix debug template to use var_export which can handle circular references. Add ast templates list to readme [31 hours ago]  

## Feb 6, 2024
I created a new repo, Builder, (see https://gitlab.com/taeluf/php/builder) for the phar generation, version bumping, and git tag creation. It is a work in progress. I copied the VersionBumper.php code over to it (and modified some of it), but haven't copied the other code (just bin scripts, i think) yet.

My goal is to move the versioning/tagging/phar building into that tlfbuild project & then for code scrawl to use the build project's .phar file to handle its own builds.

## Feb 5, 2024 THOUGHTS
Code Scrawl essentially performs two functions:
1. Find `@verb()`s and replace them with text.
2. Generate files depending on configs.

Everything else is in support of those two actual features.

So then we must:
- Find & replace `@verb()`s
- Allow `@verb()` handlers to be defined
- Allow configuration handlers to be defined
- Determine where we're reading from & where we're writing to
- Provide an API for:
    - outputting files (so extensions only have to think about relative paths WITHIN the documents directory)
    - hooking into verbs
    - hooking into configurations
    - reading documentation source files
    - reading repository files

It would be nice to provide:
- An API for:
    - parsing repository files into ASTs
    - Additional information about repository files
    - File docblock in repository/code files
    - Data defined in .json, .yaml, or other formats

Code Scrawl does most of this already. I don't think its very well documented. I think the above is a good outline for HOW to document it. The code base is kind of a confusing mish-mash, I think ... but yeah probably just need to document well.

And it's confusing that the current setup treats AST generation as a core feature, and tries to provide extensibility around it. I think it would be much cleaner to just have a simple API for querying about ASTs, rather than classes implementing interfaces that respond to certain "events".

Also, should there be a general purpose events system? I think it might be better if not. Perhaps each extension should be fully responsible for itself. Hmm. Idk.

## Feb 4, 2024 Evening
- Implemented a much better get-new-version, which uses the new VersionBumper class, and there is a test in test/run/Main.php
- Couldn't figure out how to get git push to work from inside the gitlab cicd environment. I found a forum at https://forum.gitlab.com/t/git-push-from-inside-a-gitlab-runner/30554/3 that seems to have the solution. I also found a promising article, but I barely even skimmed it: https://www.benjaminrancourt.ca/how-to-push-to-a-git-repository-from-a-gitlab-ci-pipeline/ 

## Feb 4, 2024
Yesterday, I setup scripts:
- `build-phar`: Use phar-composer.phar to generate a pharchive of code scrawl.
- `get-new-version`:  For `X.Y.Z`, use current branch, last commit message, & available tag names to determine the next tag/release version
- `tag-version`: Checkout a new local build branch, build the phar, add it to the bin dir & commit, and create a remote versioned tag (*version name comes from `get-new-version`*). Then switches back to the previous branch & deletes the local build branch

Today: 
- Improve `bin/build-phar` to use `phar-composer.phar` file instead of `phar-composer-1.4.0.phar` & now uses `php phar-composer.phar --version` to verify which version is being used. Incorrect version can be used with appropriate flags. See docs in the file.
- Added above notes

Notes:
- get-new-version needs to be made complete, with version bumps based upon commit messages. For format `X.Y.Z`, a `bugfix:` prefix should increse Z by one. A `feature:` prefix should increase Y by one. X should only ever be incremented because the branch name has changed. For 0.Y.Z, All commits should increase `Z` only. OR the 0.## format should be `0.X.Y.Z`. And perhaps the non-zero `X.Y.Z` could become `X.Y.Z.O` where `O` represents ANY OTHER CHANGE. X is backward compatibility break. Y is new feature. Z is new bugfix. And perhaps a new tag should NOT be generated unless one of there is a valid `prefix:` in the commit message. Hmm. Idk
- Once I have all the versioning-related decisions made & implemented, I should try to refactor my scripts & get them working in cicd


## Feb 3, 2024
I tried setting up cicd. its a mess. i don't understand. I'm stuck on the part of adding a .phar archive to my release (oh, maybe I should look at an existing project's cicd pipeline & copy what they're doing. What major projectsa re on gitlab?)


## Dec 23, 2023
This stuff:
* 7535260 - (HEAD -> v0.8, origin/v0.8) run scrawl (4 minutes ago) <Reed Sutman>
* c119802 - Fix url path for class documentation, so that any double forward-slashes are removed. (4 minutes ago) <Reed Sutman>
* ed30a63 - add git/ChangeLog template & a changelog documentation src file (template for everybody. src file for itself) (44 minutes ago) <Reed Sutman>
* 6c61bbe - Add api.generate_readme config (default TRUE). This is NEW Functionality that creates a README in the api/ dir. Created template for api readme generation & implemented it for Code Scrawl's docs (53 minutes ago) <Reed Sutman>
* de02bb5 - add api.output_dir configuration. Default avlue has same functionality as before (always output API to doc/api/ dir. Set null/false in your config to disable api output). (2 hours ago) <Reed Sutman>
* fd071e4 - other fixes. run scrawl. No errors any more! (2 hours ago) <Reed Sutman>
* 4bc75e1 - minor pathing fixes. run scrawl (2 hours ago) <Reed Sutman>
* 8a59b35 - move files. code/ -> src/, .docsrc / -> docsrc/, .config/ -> config/, .doctemplate -> doctemplate/ (2 hours ago) <Reed Sutman>

## Dec 20, 2022
- documented architecture
- made `$scrawl->mdverb_ext` accessible to bootstrap files
- `file.bootstrap` is now `require`d at start of `run()` so `$scrawl` can ALSO be accessed via `$this`.
- TODO: in scrawl->run(), add a simple hooks / extensions mechanism. Call hooked methods (or objects) on each source code file, and on each documentation source file, and at the end of run(). Possible other intermediate calls.
- TODO: Use `@‌template('all_classes')` or something similar to show the architecture. (need to write their docblocks and also change the template)
- TODO: In `.doctemplate/Scrawl/Templates.md.php`, add docblock descriptions, at least, to output

## Dec 18, 2022
- It doesn't look like I have a way to dynamically add php extension classes (such as the bash classes). See the Dec 10 notes for more information.
- Updated README to have a better flow & better detailed documentation
- TODO: Improve documentation of how to write extensions
- TODO: Setup dynamically adding php extension classes.
- TODO: Re-add `scrawl init` bin function

## Dec 10, 2022
- Extensions! I don't seem to have a way to set extensions in the config.json, since the only place extensions are dynamically checked for is in `parse_str()` & it expects an already instantiated object. in `Integrate.php::testPhpExtWithScrawl()`, extensions are dynamically added, but this isn't through configs or through the CLI. I'd like to fix this, so I can write extensions in other packages, then add them to the config, then have them run like they're supposed to. One solution is to add a simple callback function that can do things dynamically. So, when `Scrall->run()` is called, I could check the config for a `setup_file` or `setup_function`, then that file or function could setup scrawl however it needs to be setup. That allows more configurability, since it gives PHP access to scrawl when scrawl is run through CLI, and it is simpler because I don't have to come up with a new scheme for extension setup.

## TODO (apr 27, 2022)
- review old notes?? I think i already did the new structure? idk

## Goals
- rewrite scrawl to the new structure
- add an annotations feature that extracts all `@tags` from docblocks & comments throughout the code base & links them to asts (and/or files, maybe? idk)

## Versions
Lexing (ast generation) is not really ready
- v0.3 depends on lexer v0.6, which is abandoned but stable??
- v0.4 depends on lexer v0.2, which supports SIMPLE bash lexing & SIMPLE php lexing
- v0.5 depends on lexer v0.2, which is under development & has several failing tests
- v0.6 depends on lexer v0.7, the up & coming lexer
- v0.7 less extensible, much simpler codebase

## TODO
- IMPORTANT re-run `scrawl` on this repo & review it for errors & clean up the documentation
- IMPORTANT update default branch for code scrawl
- PLEASE write test for `scrawl init` (likely requires some internal changes)
- PLEASE make it so global `scrawl` command can successfully run `vendor/bin/scrawl`
- MAYBE add `@verb` escaping like `\@verb` or `@\verb` (probably `\@verb`)
- EH reorg some of the classes
- EH rework some of the naming
- EH rework some of the defaults
- MEH clean up the `bin/scrawl` script
- MEH review the `code/Old.php` file for ... stuff i might actually use
- MEH review the `old/GeneratedDocs.php` test class

## Latest
- Feb 15, 2022:
    - integration test for `scrawl->run()` (and write `run()`)
    - add integration test for running scrawl from cli (and modify `bin/scrawl`)
    - clean up the repo
- feb 14, 2022: 
    - api dir generation
    - all_classes template

## Feb 11, 2022 end of day
I was working on generating the `Api` folder ... prototyping it in a test `phptest -test generateApiDir`

i want to finish that prototype
then i want to make a more integrated test that basically puts that prototype into a more generic function on scrawl or the php ext ... idk

then i also want to do the `all_classes` template ... which would still require me to set the file ast on every class ... which would ... require additional processing, maybe setting file ast's to Scrawl or by pre-processing classes and adding file path to their asts before separating them FROM their file asts or by adding references back to their parent ast ... idk about that one

And once those "two" things are done, i'd like to write an integration test that ... runs code scrawl on a project directory (likely from the `test/input/` dir somewhere) ... 

THEN i can update to use the cli runner with that integrated code

THEN i can review old code & delete almost all of it

Woo heck hoo!

have a good time reed! Don't spend too much time on unimportant software. There are other priorities. This is important (to me), but far from crucial & probably not that important for the world ... but maybe the future will bear out something different & I'll find out that my awesomse software is awesome lol idk.

Love,
Reed <3


## Feb 11, 2022 (near end of day)
goals:
1. test all_classes template
2. write api docs to disk for every php file in the code dirs, matching the directory structure on disk

## Feb 10, 2022 end of day
TODO:
- NO write export list ext
- NO all docblocks ext? idk ... i might just skip it ...
- test all my templates

DONE:
- ast verb
- moved SimpleVerbs to new structure. Wrote tests for them
- cleaned up the repo (stuff i've covered already or that i don't need).
- 

Notes:
- code-old.bak is code-old before i started deleting things ... i really shouldn't need any of it
- test/old/ is ... old tests?? i think they might be useful. should review them


## Feb 10, 2022
I made the new branch ... and made notes on v0.6 ...
i want to just ... clean this up, i think ... remove all the old trash code so i can start centering around the new, good code ... then start rewriting and stuff

## Feb 8, 2022
- I've started a rewrite ... i'll need to make a new branch ... i'm still needing to finish `New/Ext/ExportStartEnd.php` and its tests ... Then I need to modify the existing rewrites & use the new version of BetterReg (Breg) ... just look at `code/New` & `test/run/Idk.php`

## Jan 28, 2022
I would really like to index all the `@whatevers` throughout comments & docblocks. Then be able to like, `$scrawl->get('feature', 'PhpFileHandler')` and have a reference to the docblock & property/method/class/etc on which `@feature(PhpFileHandler)` is declared

## Jan 25, 2022
- i updated the lexer integration code & the `classList` template so api docs are now being generated ... though still missing many important features

## Dec 6, 2021 (end of day):
TODO:
- after review, set this as default branch
- code/Scrawl.php needs major refactor (because it's just confusing)
- Extension stuff needs major refactor (because it's juts confusing)
- if config file does not exist, then prompt/ask if init is desired

NOTES:
- tests are passing
    - autoloading MIGHT not be working correctly ... I'm not sure
- cli implementation is totally based upon cli lib now
- composer.json is correct
- nothing lexer/ast related has been reviewed
- `scrawl` and `scrawl init` both work
- `.doctemplate` is implemented
- Docs are largely written

Extra TODO:
- test each template (mainly just to have example usage)


## v0.6: Up & coming scrawl
Just made new branch. TODO:
- general cleanup
- setup new cli lib
- fix all tests


---

## Copied from ... v0.4? Pretty sure
Basically, its in a very messy state and it needs some real time & attention to clean everything up.

- My lexer isn't properly parsing php. I need to fix this. Especially look at Grammar.php's ast. It IS getting `onLexerStart()` method, but its being recorded as a function in the file, not a method of the class.
- I just started updating the `verbAst` function (old implementation for `@ast()`), but didn't finish. Right now I just have it calling the method used for `@ast_class()`, but I haven't confirmed it due to the aforementioned lexer issues
- My templates verb function also needs updated to the latest version.

### Ideas
- shorthand extension declaration for built-in extensions.
    - `Lang\\*` to load all language extensions
    - `MdVerb\\Ast` for Ast extension (instead of `\Tlf\Scrawl\Ext\MdVerb\Ast`)
- Remove the `key=>value` pairing & switch to pure array of class names (and shorthands)
- a `disable` key to disable certain extensions that are automatically setup. 
- `@ast(type, key, astprop.whatever)`. Example: `@ast(class, \Tlf\Scrawl, methods.__construct.docblock)` will print a `trim`med docblock into the md file
- `@todo()` to print all TODOs found in the code base
    - `@TODO(*)` to output all TODOs (or something like it)
    - `@TODO(rel/file.ext)` to output all TODOs from `rel/file.ext`
- Configs for what generated output files should be written
- `@see_class(ClassName)` to output `[FqnClassName](LinkToClassFile)`
- `@see_function(FunctionName)` to output `[function_name()](LinkToFunctionFile)`
- `@see_file(Rel/FileName)` to output a link to the file (allows scrawl to report errors, so you know the link stays valid)
- `filename.src.md.php` to execute as php file, THEN run code scrawl
    - `filename.src.php.md` to run code scrawl THEN execute as php file
- `@see_dependencies()` to show links to all dependencies (as declared in composer.json)
- add optional link checking (`curl` every link to make sure they are valid)


### Latest (newest to oldest)
- changed `lex` setting to `lex.php` and `lex.bash` and now it defaults to `false`
- `@ast_class` works! `@ast` does too, but its currently just an alias for @ast_class
- add error reporting via `$scrawl->error('header', $msg)`
- Modify mdverb regex to allow `@verb()s` anywhere, not just at the start of a line. & remove requirement for rest of line to be whitespace.
- add `@easy_link(twitter, TaelufDev)` mdverb.
- Added `composer_install` template
- Added `@template(template_name, ...args)` as part of `Md/SimpleVerbs` extension
- Add `lex` config to conditionally disable the lexer.
- Md verb methods now receive a list of args instead of an argListString. The about the match is passed in an array as the first arg
- Vastly improved `@ast()`
- Refactored namespaces & other stuff. Much cleaned up! 
- Refactored to `interface`-driven extension system (rather than key-based)
- Clean up documentation files
- `@file(relative/path/to/file.whatever)` to include the trimmed contents of a file
- Cached lexer asts (in Lexer)
- Minor fixes
- new ApiLexer extension integrates with the PHPGrammar of my lexer library
