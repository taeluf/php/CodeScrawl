<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File ./MdVerb/MainVerbs.php  
  
# class Tlf\Scrawl\Ext\MdVerb\MainVerbs  
  
  
## Constants  
  
## Properties  
- `public \Tlf\Scrawl $scrawl;` a scrawl instance  
  
## Methods   
- `public function __construct(\Tlf\Scrawl $scrawl)`   
- `public function setup_handlers(\Tlf\Scrawl\Ext\MdVerbs $md_ext)` add callbacks to `$md_ext->handlers`  
- `public function at_template(string $templateName, ...$templateArgs)`   
- `public function at_import(string $key)` Import something previously exported with @export or @export_start/@export_end  
- `public function at_file(string $relFilePath)`   
- `public function at_see_file(string $relFilePath)`   
- `public function at_hard_link(string $url, string $name=null)` just returns a regular markdown link. In future, may check validity of link or do some kind of logging  
- `public function at_easy_link(string $service, string $target)`   
  
