<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File ./MdVerb/MdVerbs.php  
  
# class Tlf\Scrawl\Ext\MdVerbs  
Runs `@mdverb` extensions, enabling special callbacks for `@verb()`s in `.src.md` files  
  
## Constants  
  
## Properties  
- `protected $regs = [  
        'verb' =>'/(?<!\\\\)\@([a-zA-Z_]+)\(([^\)]*)\)/m',  
    ];`   
- `public $handlers = [];` `key=>value` array of verb handlers. value should be callable. key is the verb.  
  
## Methods   
- `public function get_verbs(string $str): array` Get all `@verbs(arg1, arg2)`   
- `public function replace_all_verbs(string $doc): string` Get all verbs, execute them, and replace them within the doc  
- `public function run_verb(string $src, string $verb, array $args): string` execute a verb and get its output   
  
