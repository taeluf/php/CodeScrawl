<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File ./Ext/Php.php  
  
# class Tlf\Scrawl\FileExt\Php  
Integrate the lexer for PHP files  
  
## Constants  
  
## Properties  
- `public \Tlf\Scrawl $scrawl;`   
  
## Methods   
- `public function __construct(\Tlf\Scrawl $scrawl)`   
- `public function get_all_classes()`   
- `public function set_ast(array $ast)`   
- `public function parse_str(string $str): array` Parsed `$str` into an ast (using the Lexer)  
- `public function parse_file(string $file_path): array` Parsed `$str` into an ast (using the Lexer)  
  
- `public function make_docs(array $ast)` use the ast to create docs using the classList template  
  
  
