<?php

namespace Tlf\Scrawl\Test;


/**
 *
 */
class Main extends \Tlf\Tester {

    /**
     *
     * @test that sample files get copied from test/input/Project/* to test/input/Init/*
     */
    public function testInit(){
        $dir = dirname(__DIR__).'/input/Init/';
        $this->empty_dir($dir, true);

        $args = json_decode(file_get_contents($dir.'/../Project/.config/scrawl.json'),true);
        $cli = new \Tlf\Cli();
        $cli->pwd = $dir;
        $args['noprompt'] = true;
        $scrawl = new \Tlf\Scrawl($dir, $args);
        $scrawl->run_init($cli, $args);


        $target_files = \Tlf\Tester\Utility::getAllFiles(dirname($dir).'/Project/', dirname($dir).'/Project/');

        $target_files = array_filter($target_files, function($f){return substr($f,0,6)!='/docs/';});

        $actual_files = \Tlf\Tester\Utility::getAllFiles($dir, $dir);


        sort($target_files);
        sort($actual_files);

        // $actual_files= array_map(function($f){return $f->relPath;}, $actual_files);
        $this->compare($target_files, $actual_files);



    }

}
