<?php

namespace Tlf\Scrawl\Test;

class Integrate extends \Tlf\Tester {

    /**
     * @param $which `run-full` or `run-cli` jsut to pick the right dirs
     */
    public function check_full_test(string $which){
        $dir_docs = $this->file('test/output/'.$which.'/docs/');
        $dir_root = $this->file('test/input/'.$which.'/');
        $files = \Tlf\Scrawl\Utility\Main::allFilesFromDir($dir_docs,'');
        // print_r($files);
        $this->compare_arrays(
            [
            '/api/code/abc/two.php.md',
            '/api/code/One.php.md',
            '/AllClasses.md',
            '/README.md',
            '/One.md',
            ],
            $files
        );

        $this->test('readme file output');
        $this->str_contains(
            file_get_contents($dir_docs.'/README.md'),
            'composer require taeluf/code-scrawl v0.8.x-dev',
            'class One {',
            'this is a test template',
            'test arg--second arg',
            '<!-- MdVerb `@non_existent_verb()` has no handler -->'
        );

        $this->test('copy readme file from docs to root');
        $this->compare(
            file_get_contents($dir_docs.'/README.md'),
            file_get_contents($dir_root.'/README.md')
        );

        $this->test('@ast verb');
        $this->str_contains(
            file_get_contents($dir_docs.'/One.md'),
            'public function go()',
        );

        $this->test('all classes template');
        $this->str_contains(
            file_get_contents($dir_docs.'/AllClasses.md'),
            '## two',
            '## One',
        );

        $this->test('Api output');
        $this->str_contains(
            file_get_contents($dir_docs.'/api/code/One.php.md'),
            '# File code/One.php',
            '- `public function go()` test docblock for go()',
            '- `public function test_2()` test docblock for test_2()',
        );

    }

    public function testRunCli(){
        $this->empty_dir($this->file('test/output/run-cli/docs/'), true);
        $readme_file = $this->file('test/input/run-cli/README.md');
        if (file_exists($readme_file))unlink($readme_file);

        $file = $this->file('test/input/run-cli/');
        $cli = $this->file('bin/scrawl');
        $cmd = "cd '$file'; '$cli';";
        $out = system($cmd);

        echo $out;

        $this->check_full_test('run-cli');
    }

    /**
     *
     * @test md verbs
     * @test @ast mdverb
     * @test all classes template
     * @test copy readme
     * @test custom mdverb handler
     * @test custom template
     * @test custom mdverb handler NOT FOUND
     */
    public function testRunFull(){
        $this->empty_dir($this->file('test/output/run-full/docs/'), true);
        $readme_file = $this->file('test/input/run-full/README.md');
        if (file_exists($readme_file))unlink($readme_file);

        $scrawl = new \Tlf\Scrawl(
            ['dir.root'=>$this->file('test/input/run-full/'),
            'dir.docs'=>$this->file('test/output/run-full/docs/'),
            'dir.src'=>$this->file('test/input/run-full/docsrc/'),
            'dir.scan'=>['code'],
            'template.dirs'=>[$this->file('test/input/run-full/template/')],
            ]
        );

        $scrawl->verb_handlers['own_verb'] = 
            function($arg1, $arg2){
                return $arg1.'--'.$arg2;

            };

        $scrawl->run();
        
        $this->check_full_test('run-full');
    }

    /**
     * @test Scrawl->get_all_classes()
     * @test all_classes template
     */
    public function testAllClasses(){
        $scrawl = new \Tlf\Scrawl();
        $scrawl->dir_root = $this->file('test/input/api-full/');
        $scrawl->dir_docs = $this->file('test/output/api-full/');
        $scrawl->dir_scan = [
            '.',
        ];
        $classes = $scrawl->get_all_classes();

        $class_names = [
            'Tlf\Scrawl\Utility\DocBlock',
            'Tlf\Scrawl\Utility\Main',
            'Tlf\Scrawl\Utility\Regex',
            'Tlf\Scrawl\FileExt\ExportDocBlock',
            'Tlf\Scrawl\FileExt\ExportStartEnd',
            'Tlf\Scrawl\Ext\Main',
            'Tlf\Scrawl\FileExt\Php',
            'Tlf\Scrawl\Ext\MdVerb\Ast',
            'Tlf\Scrawl\Ext\MdVerb\MainVerbs',
            'Tlf\Scrawl\Ext\MdVerbs',
            'Tlf\Scrawl'
            ];
        sort($class_names);
        $actual_class_names = array_keys($classes);
        sort($actual_class_names);
        $this->compare_arrays($class_names, $actual_class_names);


        $template = $scrawl->get_template('all_classes', [$classes]);
        $template2 = $scrawl->get_template('all_classes', []);

        $this->test('all_classes template ... classes passed in vs not');
        $this->is_true($template==$template2);

        $this->str_contains($template,
            ...$class_names
        );
    }

    /**
     * @test
     */
    public function testGenerateApiDir(){

        $scrawl = new \Tlf\Scrawl();
        $scrawl->dir_root = $this->file('test/input/api-full/');
        $scrawl->dir_docs = $this->file('test/output/api-full/');
        $scrawl->dir_scan = [
            '.',
        ];

        $this->empty_dir($scrawl->dir_docs, true);


        $scrawl->generate_apis();

        $files = \Tlf\Scrawl\Utility\Main::allFilesFromDir($scrawl->dir_docs, '');

        // print_r($files);

        $target = [
                '/api/Utility/DocBlock.php.md',
                '/api/Utility/Main.php.md',
                '/api/Utility/Regex.php.md',
                '/api/Ext/ExportDocBlock.php.md',
                '/api/Ext/ExportStartEnd.php.md',
                '/api/Ext/Main.php.md',
                '/api/Ext/Php.php.md',
                '/api/MdVerb/AstVerb.php.md',
                '/api/MdVerb/MainVerbs.php.md',
                '/api/MdVerb/MdVerbs.php.md',
                '/api/Scrawl.php.md',
            ];
        sort($target);
        sort($files);
        $this->compare_arrays($target,$files);

        $this->str_contains(
            file_get_contents($this->file('test/output/api-full/api/Scrawl.php.md')),
'- `public function report(string $msg)` Output a message to cli (may do logging later, idk)',
'- `public function warn($header, $message)` Output a message to cli, header highlighted in red',
'- `public function good($header, $message)` Output a message to cli, header highlighted in red',
'- `public function prepare_md_content(string $markdown)` apply small fixes to markdown  '
        );
    }

    /**
     * @test prototype of generating api docs
     */
    public function testGenerateApiDirPrototype(){

        $scrawl = new \Tlf\Scrawl();
        $scrawl->dir_root = $this->file('');
        $scrawl->dir_docs = $this->file('test/output/api/');
        $this->empty_dir($scrawl->dir_docs);
        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);
        $ast = $php_ext->parse_file('test/run/Integrate.php');



        $path = $ast['path'];
        $rel_path = substr($path, strlen($scrawl->dir_root));
        $ast['path'] = $rel_path;

        // $scrawl->set('ast','file.'.$rel_path, $ast);

        $classes = array_merge($ast['class'] ?? [], $ast['namespace']['class'] ?? []);

        $doc = "# File ".$rel_path."\n";
        foreach ($classes as $c){
            $markdown = $scrawl->get_template('ast/class', [null,$c,null]);

            $doc .="\n".$markdown;
        }

        // echo $doc;

        $scrawl->write_doc('test-gen_api.md', $doc);

        $this->str_contains(
            file_get_contents($this->file('test/output/api/test-gen_api.md')),
            '- `public function testGenerateApiDirPrototype()`',
            '- `public function testGetAllClasses()`',
            '- `public function testPhpExtWithScrawl()`',
        );
    }


    public function testGetAllClasses(){

        $class1 = <<<PHP
            <?php
            class Abc {
                function def(){}
            }
        PHP;
        $class2 = <<<PHP
            <?php
            class Ghi {
                function jkl(){}
            }
        PHP;
        $class3 = <<<PHP
            <?php
            class Mno{
                function pqr(){}
            }
        PHP;

        $scrawl = new \Tlf\Scrawl();
        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);
        $ast1 = $php_ext->parse_str($class1);
        $ast2 = $php_ext->parse_str($class2);
        $ast3 = $php_ext->parse_str($class3);

        $php_ext->set_ast($ast1);
        $php_ext->set_ast($ast2);
        $php_ext->set_ast($ast3);

        $classes = $php_ext->get_all_classes();

        $this->compare(
            'Abc',
            $classes['Abc']['fqn'],
        );
        $this->compare(
            'Ghi',
            $classes['Ghi']['fqn'],
        );
        $this->compare(
            'Mno',
            $classes['Mno']['fqn'],
        );

    }







    public function testPhpExtWithScrawl(){
        echo "this is an old test from the beginning of the rewrite ... probably don't need it anymore. I don't think it was ever passing";
        $this->disable();
        return;
        $str = $this->php_code;
        $scrawl = new \Tlf\Scrawl();

        $scrawl->extensions['code']['php'][] = new \Tlf\Scrawl\FileExt\Php();

        $res = $scrawl->parse_str($str, 'php');
        print_r($res);
        exit;

        print_r($scrawl->get('ast'));


        return;

        $scrawl->extensions['file']['php'];
        $scrawl->addExtension('file');

        $outputs = $scrawl->process_str($str, '.php');

        // this should have
        // ast = ... the ast ...
        // tags = 
        print_r($outputs);

        // i should use the lexer to build the ast explicitly
        $class_ast = null; 
        $method_ast = null;
        $this->compare(
            [
                'ast'=>[
                    'class'=>['Abc'=>$class_ast] // ast as from lexer
                ],
                'tags'=>[
                    'feature'=>['name'=>'no feature', 'target'=>'ast.class.Abc']
                ],
                'flat'=>[
                    'ast.class.Abc'=>$class_ast,
                    'ast.class.Abc.method.ghi'=>$method_ast,
                ],
            ],
            $outputs
        );

        return;
        // this shouldn't do anything bc i haven't added any extensions
        print_r($scrawl->getOutputs());
    }

}
