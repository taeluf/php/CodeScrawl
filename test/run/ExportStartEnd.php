<?php

namespace Tlf\Scrawl\Test;

class ExportStartEnd extends \Tlf\Tester {

    /**
     * The old preg_match_all solution was buggy and provided unexpected results in at least one case. Rather than troubleshoot the regex, I implemented a much more procedural PHP strpos looping solution. 
     */
    public function testExportsBug1(){
        $export_startend = new \Tlf\Scrawl\FileExt\ExportStartEnd();
        $input_fails = file_get_contents($this->file('test/input/bugs/export-start-end1-fails.php'));
        $input_works = file_get_contents($this->file('test/input/bugs/export-start-end1-works.php'));

        $exports_works = $export_startend->get_exports($input_works);
        $exports_fails = $export_startend->get_exports($input_fails);



        ksort($exports_works);
        ksort($exports_fails);

        $this->compare_arrays($exports_works, $exports_fails);
            
    }

}

        
