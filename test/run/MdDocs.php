<?php

namespace Tlf\Scrawl\Test;

/**
 * For testing all things `.md` file
 */
class MdDocs extends \Tlf\Tester {




    /**
     * @test getting an ast from a dot-property like `class.Abc.methods.go`
     * @test getting markdown from a dot-property & template name
     * @test parsing a doc with MdVerbs ext & auto-filling in the @ast verb
     * @test template ast/method
     * @test template ast/default
     */
    public function testAstVerb(){
        $doc = <<<MARKDOWN
            @ast(class.Abc.methods.go.docblock.description)
        MARKDOWN;
        $code = <<<PHP
            <?php
            class Abc {
                /**
                 * Test Description
                 *
                 * @param \$oh nothing
                 * @return void
                 */
                public function go(\$oh){}
            }
        PHP;


        $scrawl = new \Tlf\Scrawl();

        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);
        $ast = $php_ext->parse_str($code);

        $scrawl->set('ast','class.Abc', $ast['class'][0]);

        $ast_ext = new \Tlf\Scrawl\Ext\MdVerb\Ast($scrawl);

        $method_ast = $ast_ext->get_ast('class.Abc.methods.go');
        $this->compare(
            'method',
            $method_ast['type'],
        );
        $this->compare(
            'go',
            $method_ast['name'],
        );

       $this->compare(
            'Test Description',
            $ast_ext->get_ast('class.Abc.methods.go.docblock.description')
        );
        $this->compare_lines(
            '# Abc::go'
            ."\nTest Description",
            $ast_ext->get_markdown('class.Abc.methods.go', 'ast/method')
        );

        $this->test('main verbs integration');

        $verb_handler = new \Tlf\Scrawl\Ext\MdVerbs($scrawl);
        $verb_handler->handlers['ast'] = [$ast_ext, 'get_markdown'];

        $this->compare_lines(
            "Test Description",
            $verb_handler->replace_all_verbs($doc)  
        );

    }


    /** @test built-in md verb handlers */
    public function testMdVerbsMain(){
        $doc = <<<MARKDOWN
        @import(test)
        @file(test.txt)
        @template(test.template, one, two)
        @easy_link(tlf,php/code-scrawl)
        @hard_link(https://taeluf.com, Taeluf.com)
        @see_file(test.see.txt)
        MARKDOWN;
        $scrawl = new \Tlf\Scrawl();
        $scrawl->dir_root = $this->file('test/input/md-verbs/');
        $scrawl->template_dirs[] = $this->file('test/input/md-verbs/');
        $scrawl->set('export', 'test', 'test export');
        $ext = new \Tlf\Scrawl\Ext\MdVerbs();
        $verb_handler = new \Tlf\Scrawl\Ext\MdVerb\MainVerbs($scrawl);
        $verb_handler->setup_handlers($ext);
        ob_start();
        $output = $ext->replace_all_verbs($doc);
        $errors = ob_get_clean();

        $this->test('no error output');
        $this->compare('', $errors);

        $this->test('filled-in documentation');
        $this->compare_lines(
        <<<MARKDOWN
            test export
            test.txt file
            one-two
            [php/code-scrawl](https://tluf.me/php/code-scrawl)
            [Taeluf.com](https://taeluf.com)
            [test.see.txt](/test.see.txt)
        MARKDOWN,
            $output
        );
    }

    /** @test that \@verbs() work */
    public function testMdVerbsParsing(){
        $doc = <<<MARKDOWN
        # Whatever
        @test(one) with @test(two, three, four)
        @test(three)
        MARKDOWN;
        $ext = new \Tlf\Scrawl\Ext\MdVerbs();
        $verbs = $ext->get_verbs($doc);

        $this->compare(
            [
                ['src'=>'@test(one)',
                'verb'=>'test',
                'args'=>['one'],
                ],
                ['src'=>'@test(two, three, four)',
                'verb'=>'test',
                'args'=>['two', 'three', 'four'],
                ],
                ['src'=>'@test(three)',
                'verb'=>'test',
                'args'=>['three'],
                ],

            ],
            $verbs
        );

        $ext->handlers['test'] = function(...$strings){
            return implode(':',$strings);
        };

        $replacement = $ext->run_verb($verbs[1]['src'], $verbs[1]['verb'], $verbs[1]['args']);
        $this->compare('two:three:four', $replacement);
    }

    /** @test copying readme to PROJECT_ROOT/README.md */
    public function testCopyReadme(){
        $scrawl = new \Tlf\Scrawl(
            ['dir.docs'=> $this->file('test/input/docs/'),
            'dir.root'=>$this->file('test/input/project-root/')
            ]
        );
        $main_ext = new \Tlf\Scrawl\Ext\Main();
        $main_ext->copy_readme($scrawl);

        $this->file_exists($this->file('test/input/docs/README.md'));
    }

}
