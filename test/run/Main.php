<?php

namespace Tlf\Scrawl\Test;

class Main extends \Tlf\Tester {


    public function testIncreaseVersion(){

        // for anything >= 1.0.0
        // if 0.x.y, x depends on branch name
        // so if the prefix 'feature:' is on a commit message, then 1.0.7 becomes 1.1.0
        $nonzero_dot_rules = [
            // 0-based indexes!
            // 0 is not used, this depends on the branch
            1 => [ 'feature' ],
            2 => [ 'bugfix' ],
            3 => [ 'other' ],
        ];
        $zero_dot_rules = [
            // 0-based indexes!
            // 0 is not used, this depends on the branch
            2 => [ 'feature' ],
            3 => [ 'bugfix', 'other' ],
        ];

        $tests = [
            // latest version, commit message, bump rules, expected_new_version
            [ '0.8.0', 'bugfix: yep', $zero_dot_rules, '0.8.0.1'],
            [ '0.8.0.1', 'bugfix: yep', $zero_dot_rules, '0.8.0.2'],
            [ '0.8.6', 'bugfix: yep', $zero_dot_rules, '0.8.6.1'],
            [ '0.8.6.0', 'bugfix: yep', $zero_dot_rules, '0.8.6.1'],

            [ '0.8.0', 'feature: yep', $zero_dot_rules, '0.8.1'],
            [ '0.8.0.1', 'feature: yep', $zero_dot_rules, '0.8.1.0'],
            [ '0.8.6', 'feature: yep', $zero_dot_rules, '0.8.7'],
            [ '0.8.6.0', 'feature: yep', $zero_dot_rules, '0.8.7.0'],

            [ '0.8.1.1', 'other: yep', $zero_dot_rules, '0.8.1.2'],
        ];

        $bumper = new \Tlf\Scrawl\VersionBumper();

        foreach ($tests as $index => $test){
            $new_version = $bumper->get_new_version(
                $test[0],
                $test[1],
                $test[2],
            );

            if ($new_version == $test[3]){
                echo "\nPass: ".$test[0].' -> '.$test[3];
                ob_start();
                $this->handleDidPass(true);
                ob_get_clean();
            } else {
                echo "\nFail: ".$test[0].' -> expected '.$test[3]. ' got '.$new_version;
                ob_start();
                $this->handleDidPass(false);
                ob_get_clean();
            }
        }
    }

}
