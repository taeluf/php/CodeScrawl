<?php

namespace Lia\Test\Addon;

class Resources extends \Tlf\Tester {

    /** for documentation generation purposes */
    public function testAddCssAndJavascriptFiles(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        new \Lia\Addon\Resources($package);

        // @export_start(ResourcesAddon.AddResources)
        $resources = \Lia\Addon\Resources::from($lia); // your Liaison instance

        // Files will be concatenated and added to a singular css file
        $private_dir = dirname(__DIR__,2).'/input/private/resources/';
        $resources->addFile($private_dir.'/undeliverable-styles.css');
        $resources->addFile($private_dir.'/undeliverable-scripts.js');

        // URLs will be added as `<link>` and `<script>` tags and added to the HTML head
        $resources->addURL('/resources/page-layout.css'); // deliverable on your site
        $resources->addURL('https://cdn.example.com/my-namespace/dancing-bear.js'); // external file
        // @export_end(ResourcesAddon.AddResources)

        $scriptTags = $resources->getUrlTag('js');
        $cssTags = $resources->getUrlTag('css');
        $target_tags = 
<<<HTML
    <script type="text/javascript" src="https://cdn.example.com/my-namespace/dancing-bear.js"></script>
    <link rel="stylesheet" href="/resources/page-layout.css" />
HTML;

        $this->compare_lines($target_tags, trim($scriptTags)."\n".trim($cssTags));


        $js_code = $resources->concatenateFiles('js');
        $css_code = $resources->concatenateFiles('css');

        $target_js = file_get_contents($private_dir.'/undeliverable-scripts.js');
        $target_css = file_get_contents($private_dir.'/undeliverable-styles.css');

        $this->compare_lines($target_js, $js_code);
        $this->compare_lines($target_css, $css_code);




    }

    public function testCacheCompiledCSSFile(){
        $_SERVER['REQUEST_URI'] = '/';
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $package->dir = $this->dir().'/cache/';
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();
        $res->onPackageReady();

        $cache = new \Lia\Addon\Cache($package);
        $cache->init_lia();

        // $lia->set('cache.dir', $this->dir().'/cache/');

        $filesDir = $this->dir().'/file/';
        $content = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-4)!='.css')continue;
            $lia->addResourceFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }

        $tFile = $res->compileFilesToCache('css');
        $tStyles = implode("\n", $content);

        $this->compare(strlen($tStyles)>5, true, true);
        $this->compare($tStyles, file_get_contents($tFile));

        $this->empty_dir($this->dir().'/cache/');
    }

    public function testSortCss(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();

        $filesDir = $this->dir().'/file/';
        $content = [];
        $files = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-4)!='.css')continue;
            $files[$file] = $filesDir.'/'.$file;
            $lia->addResourceFile($filesDir.'/'.$file);
        }
        $files = [
            'b.css' => $files['b.css'],
            'a.css' => $files['a.css'],
            'c.css' => $files['c.css'],
        ];
        foreach ($files as $f){
            $content[] = file_get_contents($f);
        }

        // @export_start(ResourcesAddon.FunctionSorter)
        $lia->addon('lia:server.resources')->setSorter('css',
            /** $files is an array of absolute paths. */
            function(array $files): array{
                $files = array_values($files);
                return
                [
                    $files[1], // b.css
                    $files[0], //a.css
                    $files[2], //c.css
                ];
            }
        );
        // @export_end(ResourcesAddon.FunctionSorter)
        
        $aStyles = $res->concatenateFiles('css');
        $tStyles = implode("\n", $content);

        $this->compare(strlen($tStyles)>5, true, true);
        $this->compare($tStyles, $aStyles);
    }
    /** 
     * Test that the Resource Sorter is able to REMOVE a css file from the list.
     */
    public function testRemoveCssFile(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();

        $filesDir = $this->dir().'/file/';
        $content = [];
        $files = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-4)!='.css')continue;
            $files[$file] = $filesDir.'/'.$file;
            $lia->addResourceFile($filesDir.'/'.$file);
        }
        $files = [
            'b.css' => $files['b.css'],
            //'a.css' => $files['a.css'],
            'c.css' => $files['c.css'],
        ];
        foreach ($files as $f){
            $content[] = file_get_contents($f);
        }

        // @export_start(ResourcesAddon.FunctionRemover)
        $lia->addon('lia:server.resources')->setSorter('css',
            /** $files is an array of absolute paths. */
            function(array $files): array{
                $files = array_values($files);
                return
                [
                    $files[1], // b.css
                    // $files[0], //a.css
                    $files[2], //c.css
                ];
            }
        );
        // @export_end(ResourcesAddon.FunctionRemover)
        
        $aStyles = $res->concatenateFiles('css');
        $tStyles = implode("\n", $content);

        $this->compare(strlen($tStyles)>5, true, true);
        $this->compare($tStyles, $aStyles);
    }

    public function testCompileCss(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();


        $filesDir = $this->dir().'/file/';
        $content = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-4)!='.css')continue;
            $lia->addResourceFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }
        $aStyles = $res->concatenateFiles('css');
        $tStyles = implode("\n", $content);

        $this->compare(strlen($tStyles)>5, true, true);
        $this->compare($tStyles, $aStyles);
    }


    public function testSortJs(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();
        $filesDir = $this->dir().'/file/';
        $content = [];
        $files = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-3)!='.js')continue;
            $files[$file] = $filesDir.'/'.$file;
            $lia->addResourceFile($filesDir.'/'.$file);
        }
        $files = [
            'b.js' => $files['b.js'],
            'a.js' => $files['a.js'],
            'c.js' => $files['c.js'],
        ];
        foreach ($files as $f){
            $content[] = file_get_contents($f);
        }

        $res->setSorter('js',
            function($files){
                $files = array_values($files);
                return
                [
                    'b.js' => $files[1],
                    'a.js' => $files[0],
                    'c.js' => $files[2],
                ];
            }
        );
        
        $aScripts = $res->concatenateFiles('js');
        $tScripts = implode("\n", $content);
        $this->compare(strlen($tScripts)>5, true, true);
        $this->compare($tScripts, $aScripts);
    }

    public function testCompileJs(){
        $lia = new \Lia();
        $package = new \Lia\Package($lia, 'test');
        $res = new \Lia\Addon\Resources($package);
        $res->init_lia();
        $filesDir = $this->dir().'/file/';
        $content = [];
        foreach (scandir($filesDir) as $file){
            if (substr($file,-3)!='.js')continue;
            $lia->addResourceFile($filesDir.'/'.$file);
            $content[] = file_get_contents($filesDir.'/'.$file);
        }
        $aScripts = $res->concatenateFiles('js');
        $tScripts = implode("\n", $content);

        $this->compare(strlen($tScripts)>5, true, true);
        $this->compare($tScripts, $aScripts);
    }

    protected function dir(){
        return $this->cli->pwd . '/test/input/Resources/';
    }
}

