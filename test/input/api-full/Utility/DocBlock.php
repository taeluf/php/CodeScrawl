<?php

namespace Tlf\Scrawl\Utility;

/**
 *
 * @featured
 */
class DocBlock {


    public function __construct($rawBlock, $cleanBlock){
        $this->raw = $rawBlock;
        $this->clean = $cleanBlock;
    }

    static protected $regex = [
        // @TODO Make a less strict regex
        'DocBlock./**' => ['/((\/\*\*.*\n)( *\*.*\n)* *\*\/)/'],
        'DocBlock./**.removeBlockOpen' => '/(^\/\*\*)/',
        'DocBlock./**.removeLineOpenAndBlockClose' => '/(\n\s*\* ?\/?)/',
    ];


    static public function DocBlock($name, $match, $nullFile, $info){
        $clean = static::cleanBlock($match[0], null);
        $docBlock = new static($match[0], $clean);


        return $docBlock;
    }

    static public function extractBlocks($fileContent){
        $blocks = \Tlf\Scrawl\Utility\Regex::matchRegexes(static::class, 
                    ['DocBlock'=>static::$regex['DocBlock./**']]
                    , null, $fileContent);

        return $blocks;
    }

    static public function cleanBlock($rawBlock){
        $docBlock = 
            preg_replace(
                [   static::$regex['DocBlock./**.removeBlockOpen'], 
                    static::$regex['DocBlock./**.removeLineOpenAndBlockClose']
                ],
                ["", "\n"],
                $rawBlock
            );
        $docBlock = trim($docBlock);

        $docBlock = Main::trimTextBlock($docBlock);
        
        return $docBlock;

    }

}
