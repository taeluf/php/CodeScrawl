<?php

namespace Tlf\Scrawl\Utility;

class Regex {


    /**
     *
     */
    static public function matchRegexes($targetObject, $regexArray, File $file=null, $text=null){
        if ($file === null && $text === null)throw new \Exception("Either \$file or \$text must be set");
        if ($text === null)$text = $file->content();

        $ret = [];
        foreach ($regexArray as $name => $regs){
            // echo "\nName:$name\n\n";
            $func = $regs['function'] ?? str_replace(['-','.'], '_', $name);
            unset($regs['function']);
            foreach ($regs as $i => $regex){
                $didMatch = preg_match_all($regex, $text, $matches, PREG_SET_ORDER);
                if ($didMatch){
                    //@export_start(Regex.infoArray)
                    $info = [
                        'matchList'=>$matches, // Array of all matches
                        'lineStart' => -1, // (not implemented, @TODO) Line in file/text that your match starts on
                        'lineEnd' => -1,   // (not implemented, @TODO) Line in file/text that your match ends on
                        'text' => $text,   //
                        'regIndex' => null,
                    ];
                    //@export_end(Regex.infoArray))
                    foreach ($matches as $key => $match){
                        $info['regIndex']=$key;
                        $lineStart = 0;
                        $lineEnd = 0;
                        $info['lineStart'] = $lineStart;
                        $info['lineEnd'] = $lineEnd;
                        $ret[] = call_user_func([$targetObject, $func], $name, $match, $file, $info);
                    }
                }
            }
        }
        return $ret;
    }
}
