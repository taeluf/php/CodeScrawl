<?php
/**
 * Display composer install instructions for your library
 *
 * @usage `@template(composer_install, vendor/package, ?version)`
 *
 * @param $args[0] package name like `taeluf/code-scrawl`
 * @param $args[1] (optional) branch name ... defaults to current branch
 */


if (!isset($args[0])){
    $this->scrawl->warn("package name was not passed to @template(composer_install, vendor/package)");
    echo "--cannot print composer install instructions because package name was not passed--";
    return;
}
$package = $args[0];
$version = $args[1] ?? $ext->getCurrentBranchForComposer();

?>
```bash
composer require <?=$package.' '.$version?> 
```
or in your `composer.json`
```json
{"require":{ "<?=$package?>": "<?=$version?>"}}
```
