<?php
/**
 * @param $args[0] the key pointing to the ast, must begin with `class.ClassName`
 * @param $args[1] array class ast
 * @param $args[2] is the AstVerb class instance
 */
$Class = $class = $args[1];


// echo "\n\n\n-----------\n\n";
// var_dump($Class);
// exit;

if ($Class['type']=='class')$type = 'class';
else $type = 'trait';

$class_name = $class['fqn'] ?? $class['name'];
?>
# <?= $type .' '. $class_name ?>
<?="\n".($Class['docblock']['description']??'')?>



## Constants
<?php
foreach ($Class['const']??[] as $constant){
    $def = $constant['declaration'];
    $descript = $constant['docblock']['description']??'';
    echo "- `${def}` ${descript}\n";
}

?>

## Properties
<?php
foreach ($Class['properties']??[] as $prop){
    $def = $prop['declaration'];
    $descript = $prop['docblock']['description']??'';
    echo "- `${def}` ${descript}\n";
}

?>

## Methods 
<?php
foreach ($Class['methods']??[] as $method){
    // var_dump($method);
    // exit;
    $def = $method['declaration'];
    // $descript = $method['description'];
    $descript = $method['docblock']['description']??'';
    // var_dump($method['docblock']);
    // if (is_array($method['docblock']));
    // exit;
    echo "- `${def}` ${descript}\n";
}

?>

<?php
// static props/functions are not yet separated out by the lexer
return;

?>

## Static Properties 
<?php
foreach ($Class['staticProps']??[] as $prop){
    $def = $prop['definition'];
    $descript = $prop['description'];
    echo "- `${def}` ${descript}\n";
}

?>

## Static functions
<?php
foreach ($Class['staticFunctions']??[] as $func){
    $def = $func['definition'];
    $descript = $func['description'];
    echo "- `${def}` ${descript}\n";
}

