<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
## Readme file  
This should copy automatically  
  
```bash  
composer require taeluf/code-scrawl v0.8.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "taeluf/code-scrawl": "v0.8.x-dev"}}  
```  
  
  
```php  
<?php  
  
class One {  
  
    /** test docblock for go()   
     *  
     * @export(One.go)  
     */  
    public function go(){  
    }  
  
    /** test docblock for test_2() */  
    public function test_2(){  
    }  
}  
```  
  
this is a test template  
  
  
@own_verb(test arg, second arg)  
  
@non_existent_verb(okay)  
